<%@ page import="com.sapportals.portal.prt.event.*"%>
<%@ page import="com.sapportals.portal.prt.*" %>
<%@ page import="java.text.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.tsystems.vocosa.util.DateUtil"%>
<%@ page import="com.tsystems.controller.AsesorSolicitudFinanMMController"%>
<%@ page import="com.tsystems.controller.AsesorSolicitudFinanMMController.MODULO"%>
<%@ page import="com.tsystems.controller.AsesorSolicitudFinanMMController.ACCION"%>
<%@ page import="com.tsystems.controller.AsesorSolicitudFinanMMController.ESTATUS"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%
    IPortalRequestEventData data = componentRequest.createRequestEventData();
    IPortalRequestEvent fexitEvent = componentRequest.createRequestEvent("Content",data);
    IPortalRequestEvent guardarEvent = componentRequest.createRequestEvent("Guardar", data);
    IPortalRequestEvent actualizarEvent = componentRequest.createRequestEvent("Actualizar", data);
    IPortalRequestEvent validarDepositoImporte = componentRequest.createRequestEvent("ValidarDepositoImporte", data);
    IPortalRequestEvent cancelar = componentRequest.createRequestEvent("Cancelar", data);



    String username = request.getUserPrincipal().getName();
    SolicitudMM solicitud = (SolicitudMM) request.getAttribute("solicitudMM");
    AsesorSolicitudFinanMMController asc = AsesorSolicitudFinanMMController.getInstance();

    String url1 = componentRequest.getWebResourcePath() + "/js/jquery-1.12.0.min.js";
    String url2 = componentRequest.getWebResourcePath() + "/css/Styles.css";
    String script_numeric = componentRequest.getWebResourcePath() + "/js/jquery.numeric.min.js";
    String url3 = componentRequest.getWebResourcePath() + "/js/custom.format.number.js";
    String url4 = componentRequest.getWebResourcePath() + "/js/jquery-ui.min.js";
    String url5 = componentRequest.getWebResourcePath() + "/css/jquery-ui.min.css";
    String url6 = componentRequest.getWebResourcePath() + "/js/autoNumeric-min.js";
    String url7 = componentRequest.getWebResourcePath() + "/js/jquery.number.min.js";
    Date date = Calendar.getInstance().getTime();
    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    DecimalFormat formateador = new DecimalFormat("###,###.00");
    String today = formatter.format(date);
    DateFormat df = new SimpleDateFormat("yyyyMMdd");
%>

<script src="<%=url1%>"></script>
<script src="<%=url3%>"></script>
<script src="<%=url4%>"></script>
<script src="<%=url6%>"></script>
<script src="<%=url7%>"></script>

<link rel="stylesheet" type="text/css" href="<%= url2 %>">
<link rel="stylesheet" type="text/css" href="<%= url5 %>">


<script src="<%=script_numeric%>"></script>

<style type="text/css">
    .center {
        text-align: center;
    }
    table,tr,th,td,select,input,form,li,p,textarea,a,h4 {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: xx-small;
    }
    thead td {
        text-align: center;
        background: #c1c1c1;
        padding: 5px 12px 5px 12px;
    }
    tbody td.flex{
        text-align: right;
        display: flex;
        flex-direction: column;
        align-content: stretch;
    }
</style>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.tsystems.identity.SolicitudMM"%>
<%@page import="com.tsystems.vocosa.action.CapturaSolicitudMM"%>
<%@page import="com.tsystems.vocosa.action.EdicionSolicitudMM"%>
<%@page import="com.tsystems.controller.AsesorSolicitudFinanMMController"%>
<%@page import="com.tsystems.controller.AsesorSolicitudFinanMMController.ESTATUS"%>
<%@page import="com.tsystems.controller.AsesorSolicitudFinanMMController.ACCION"%>
<%@page import="com.tsystems.controller.AsesorSolicitudFinanMMController.MODULO"%>
<%@page import="com.tsystems.controller.AsesorSolicitudFinanMMController.MODULO"%>
<%@page import="com.tsystems.controller.AsesorSolicitudFinanMMController.MODULO"%><html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<table width="60%" border="0" align="center">
    <tr>
        <td width="20%"><%=username%></td>
        <td width="60%">
            <h4 class="center">VOLKSWAGEN LEASING S.A. DE C.V.</h4>
            </div>
        </td>
        <td align="right" width="20%"><%=today%></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>
            <%if(asc.getModuloActual()== MODULO.EVALUAR_PENDIENTE){ %>
            <h4 class="center">EVALUACIÓN DE SOLICITUD DE FINANCIAMIENTO MULTIMARCA</h4>
            <%} if(asc.getModuloActual()== MODULO.CONSULTA_RECHAZADOS){ %>
            <h4 class="center">SOLICITUD DE FINANCIAMIENTO MULTIMARCA RECHAZADA</h4>
            <%} if(asc.getModuloActual()== MODULO.AUTORIZADA_DEP_REALIZADO){ %>
            <h4 class="center">SOLICITUD DE FINANCIAMIENTO MULTIMARCA AUTORIZADA Y DEPÓSITO REALIZADO</h4>
            <%} if(asc.getModuloActual()== MODULO.SOLICITUD_CANCELADA){ %>
            <h4 class="center">SOLICITUD DE FINANCIAMIENTO MULTIMARCA SOLICITUD CANCELADA</h4>
            <%} if(asc.getModuloActual()== MODULO.PENDIENTE_DISP_INSUFICIENTE){ %>
            <h4 class="center">SOLICITUD DE FINANCIAMIENTO PENDIENTE POR DISPONIBILIDAD INSUFICIENTE</h4>
            <%} if(asc.getModuloActual()== MODULO.AUTORIZADA_ESPERA_DEP_IMPORTE){ %>
            <h4 class="center">SOLICITUD DE FINANCIAMIENTO MULTIMARCA AUTORIZADA EN ESPERA DE DEPÓSITO DEL IMPORTE</h4>
            <%} %>
        </td>
        <td>&nbsp;</td>
    </tr>
</table>

<form id="alta" class="center" method="post">
    <input type="hidden" id="username" name="username" value="<%=username%>">
    <input type="hidden" id="VALIDACION_CFDI" name="VALIDACION_CFDI" value="">
    <input type="hidden" id="EVALUACION_SOLICITUD" name="EVALUACION_SOLICITUD" value="">
    <input type="hidden" id="FOLIO" name="FOLIO" value='<%=(null!=solicitud && null!=solicitud.getFolio())? solicitud.getFolio():"" %>'>

    <table align="center" border="1">
        <thead>
        <td style="min-width:170px;">Campos</td>
        <td style="width:320px;">Valores registrados</td>
        <%if(asc.getModuloActual()== MODULO.EVALUAR_PENDIENTE || asc.getModuloActual()== MODULO.CONSULTA_RECHAZADOS){ %>
        <td style="width: 275px;">Comentarios Asesor VWL</td>
        <%} %>
        </thead>
        <tbody>
        <tr>
            <td># de Solicitud</td>
            <td style="text-align: right;"><%=(null!=solicitud && null!=solicitud.getFolio())? String.format("%08d", solicitud.getFolio()):"" %></td>
            <%if(asc.getModuloActual()== MODULO.EVALUAR_PENDIENTE || asc.getModuloActual()== MODULO.CONSULTA_RECHAZADOS){ %>
            <td></td>
            <%} %>
        </tr>
        <tr>
            <td>Distribuidor</td>
            <td style="text-align: right;"><%=(null!=solicitud && null!=solicitud.getDistribuidor())? solicitud.getDistribuidor():"" %></td>
            <%if(asc.getModuloActual()== MODULO.EVALUAR_PENDIENTE || asc.getModuloActual()== MODULO.CONSULTA_RECHAZADOS){ %>
            <td></td>
            <%} %>
        </tr>
        <tr>
            <td>Agencia</td>
            <td style="text-align: right;"><%=(null!=solicitud && null!=solicitud.getConcesion())? solicitud.getConcesion():"" %></td>
            <%if(asc.getModuloActual()== MODULO.EVALUAR_PENDIENTE || asc.getModuloActual()== MODULO.CONSULTA_RECHAZADOS){ %>
            <td></td>
            <%} %>
        </tr>
        <tr>
            <td>Fecha de Solicitud</td>
            <td style="text-align: right;"><%=(null!=solicitud && null!=solicitud.getFechaPrimerEnvio())? formatter.format(solicitud.getFechaPrimerEnvio()):"" %></td>
            <%if(asc.getModuloActual()== MODULO.EVALUAR_PENDIENTE || asc.getModuloActual()== MODULO.CONSULTA_RECHAZADOS){ %>
            <td></td>
            <%} %>
        </tr>
        <tr>
            <td>Marca</td>
            <td style="text-align: right;"><%=(null!=solicitud && null!=solicitud.getMarcaDescripcion())? solicitud.getMarcaDescripcion():"" %></td>
            <%if(asc.getModuloActual()== MODULO.EVALUAR_PENDIENTE || asc.getModuloActual()== MODULO.CONSULTA_RECHAZADOS){ %>
            <td></td>
            <%} %>
        </tr>
        <tr>
            <td>Número de factura</td>
            <td style="text-align: right;"><%=(null!=solicitud && null!=solicitud.getFactura())? solicitud.getFactura():"" %></td>
            <%if(asc.getModuloActual()== MODULO.EVALUAR_PENDIENTE || asc.getModuloActual()== MODULO.CONSULTA_RECHAZADOS){ %>
                <% if(asc.getModuloActual() == MODULO.EVALUAR_PENDIENTE){ %>
                     <td class="flex"><input type="text" maxlength="40" name="COMENTA_FACTURA"/></td>
                <%} if(asc.getModuloActual() == MODULO.CONSULTA_RECHAZADOS){ %>
                     <td style="text-align: right;"><%=solicitud.getComentaFactura()==null?"":solicitud.getComentaFactura() %></td>
                <%} %>
            <%} %>
        </tr>
        <tr>
            <td>UUID de factura</td>
            <td style="text-align: right;"><%=(null!=solicitud && null!=solicitud.getUuid())? solicitud.getUuid():"" %></td>
            <%if(asc.getModuloActual()== MODULO.EVALUAR_PENDIENTE || asc.getModuloActual()== MODULO.CONSULTA_RECHAZADOS){ %>
            <% if(asc.getModuloActual() == MODULO.EVALUAR_PENDIENTE){ %>
            <td class="flex"><input type="text" maxlength="40" name="COMENTA_UUID"/></td>
            <%} if(asc.getModuloActual() == MODULO.CONSULTA_RECHAZADOS){ %>
            <td style="text-align: right;"><%=solicitud.getComentaUuid()==null?"":solicitud.getComentaUuid() %></td>
            <%} %>
            <%} %>
        </tr>
        <tr>
            <td>Importe factura (con IVA) MXN</td>
            <td style="text-align: right;"><%=(null!=solicitud && null!=solicitud.getImporte())? formateador.format(solicitud.getImporte()):"" %></td>
            <%if(asc.getModuloActual()== MODULO.EVALUAR_PENDIENTE || asc.getModuloActual()== MODULO.CONSULTA_RECHAZADOS){ %>
            <% if(asc.getModuloActual() == MODULO.EVALUAR_PENDIENTE){ %>
            <td class="flex"><input type="text" maxlength="40" name="COMENTA_IMPORTE"/></td>
            <%} if(asc.getModuloActual() == MODULO.CONSULTA_RECHAZADOS){ %>
            <td style="text-align: right;"><%=solicitud.getComentaImporte()==null?"":solicitud.getComentaImporte() %></td>
            <%} %>
            <%} %>
        </tr>
        <tr>
            <td>Fecha de emisión de la factura</td>
            <td style="text-align: right;"><%=(null!=solicitud && null!=solicitud.getFechaFactura())? formatter.format(solicitud.getFechaFactura()):"" %></td>
            <%if(asc.getModuloActual()== MODULO.EVALUAR_PENDIENTE || asc.getModuloActual()== MODULO.CONSULTA_RECHAZADOS){ %>
            <% if(asc.getModuloActual() == MODULO.EVALUAR_PENDIENTE){ %>
            <td class="flex"><input type="text" maxlength="40" name="COMENTA_FEC_FACT"/></td>
            <%} if(asc.getModuloActual() == MODULO.CONSULTA_RECHAZADOS){ %>
            <td style="text-align: right;"><%=solicitud.getComentaFechaFactura()==null?"":solicitud.getComentaFechaFactura() %></td>
            <%} %>
            <%} %>
        </tr>
        <tr>
            <td>Número de VIN</td>
            <td style="text-align: right;"><%=(null!=solicitud && null!=solicitud.getChasis())? solicitud.getChasis():"" %></td>
            <%if(asc.getModuloActual()== MODULO.EVALUAR_PENDIENTE || asc.getModuloActual()== MODULO.CONSULTA_RECHAZADOS){ %>
            <% if(asc.getModuloActual() == MODULO.EVALUAR_PENDIENTE){ %>
            <td class="flex"><input type="text" maxlength="40" name="COMENTA_CHASIS"/></td>
            <%} if(asc.getModuloActual() == MODULO.CONSULTA_RECHAZADOS){ %>
            <td style="text-align: right;"><%=solicitud.getComentaChasis()==null?"":solicitud.getComentaChasis() %></td>
            <%} %>
            <%} %>
        </tr>
        <tr>
            <td>Modelo</td>
            <td style="text-align: right;"><%=(null!=solicitud && null!=solicitud.getModelo())? solicitud.getModelo():"" %></td>
            <%if(asc.getModuloActual()== MODULO.EVALUAR_PENDIENTE || asc.getModuloActual()== MODULO.CONSULTA_RECHAZADOS){ %>
            <% if(asc.getModuloActual() == MODULO.EVALUAR_PENDIENTE){ %>
            <td class="flex"><input type="text" maxlength="40" name="COMENTA_MODELO"/></td>
            <%} if(asc.getModuloActual() == MODULO.CONSULTA_RECHAZADOS){ %>
            <td style="text-align: right;"><%=solicitud.getComentaModelo()==null?"":solicitud.getComentaModelo() %></td>
            <%} %>
            <%} %>
        </tr>
        <tr>
            <td>Versión</td>
            <td style="text-align: right;"><%=(null!=solicitud && null!=solicitud.getVersion())? solicitud.getVersion():"" %></td>
            <%if(asc.getModuloActual()== MODULO.EVALUAR_PENDIENTE || asc.getModuloActual()== MODULO.CONSULTA_RECHAZADOS){ %>
            <% if(asc.getModuloActual() == MODULO.EVALUAR_PENDIENTE){ %>
            <td class="flex"><input type="text" maxlength="40" name="COMENTA_VERSION"/></td>
            <%} if(asc.getModuloActual() == MODULO.CONSULTA_RECHAZADOS){ %>
            <td style="text-align: right;"><%=solicitud.getComentaVersion()==null?"":solicitud.getComentaVersion() %></td>
            <%} %>
            <%} %>
        </tr>
        <tr>
            <td>Año</td>
            <td style="text-align: right;"><%=(null!=solicitud && null!=solicitud.getAnio())? solicitud.getAnio():"" %></td>
            <%if(asc.getModuloActual()== MODULO.EVALUAR_PENDIENTE || asc.getModuloActual()== MODULO.CONSULTA_RECHAZADOS){ %>
            <% if(asc.getModuloActual() == MODULO.EVALUAR_PENDIENTE){ %>
            <td class="flex"><input type="text" maxlength="40" name="COMENTA_ANIO"/></td>
            <%} if(asc.getModuloActual() == MODULO.CONSULTA_RECHAZADOS){ %>
            <td style="text-align: right;"><%=solicitud.getComentaAnio()==null?"":solicitud.getComentaAnio() %></td>
            <%} %>
            <%} %>
        </tr>
        <tr>
            <td>Nuevo o Usado</td>
            <td style="text-align: right;"><%=(null!=solicitud && null!=solicitud.getCategoriaDescripcion())? solicitud.getCategoriaDescripcion():"" %></td>
            <%if(asc.getModuloActual()== MODULO.EVALUAR_PENDIENTE || asc.getModuloActual()== MODULO.CONSULTA_RECHAZADOS){ %>
            <% if(asc.getModuloActual() == MODULO.EVALUAR_PENDIENTE){ %>
            <td class="flex"><input type="text" maxlength="40" name="COMENTA_CATEGO"/></td>
            <%} if(asc.getModuloActual() == MODULO.CONSULTA_RECHAZADOS){ %>
            <td style="text-align: right;"><%=solicitud.getComentaCategoria()==null?"":solicitud.getComentaCategoria() %></td>
            <%} %>
            <%} %>
        </tr>
        <tr>
            <td>Nombre de archivo PDF</td>
            <td style="text-align: right;"><%=(null!=solicitud && null!=solicitud.getPdfFileName())? solicitud.getPdfFileName():"" %></td>
            <%if(asc.getModuloActual()== MODULO.EVALUAR_PENDIENTE || asc.getModuloActual()== MODULO.CONSULTA_RECHAZADOS){ %>
            <% if(asc.getModuloActual() == MODULO.EVALUAR_PENDIENTE){ %>
            <td class="flex"><input type="text" maxlength="40" name="COMENTA_PDF"/></td>
            <%} if(asc.getModuloActual() == MODULO.CONSULTA_RECHAZADOS){ %>
            <td style="text-align: right;"><%=solicitud.getComentaPdf()==null?"":solicitud.getComentaPdf() %></td>
            <%} %>
            <%} %>
        </tr>
        <tr>
            <td>Nombre de archivo XML</td>
            <td style="text-align: right;"><%=(null!=solicitud && null!=solicitud.getXmlFileName())? solicitud.getXmlFileName():"" %></td>
            <%if(asc.getModuloActual()== MODULO.EVALUAR_PENDIENTE || asc.getModuloActual()== MODULO.CONSULTA_RECHAZADOS){ %>
            <% if(asc.getModuloActual() == MODULO.EVALUAR_PENDIENTE){ %>
            <td class="flex"><input type="text" maxlength="40" name="COMENTA_XML"/></td>
            <%} if(asc.getModuloActual() == MODULO.CONSULTA_RECHAZADOS){ %>
            <td ><%=solicitud.getComentaXml()==null?"":solicitud.getComentaXml() %></td>
            <%} %>
            <%} %>
        </tr>
        </tbody>
    </table>

    <% if(asc.getModuloActual() == MODULO.EVALUAR_PENDIENTE || asc.getModuloActual() == MODULO.CONSULTA_RECHAZADOS){ %>
        <div style="width:851px; margin:auto;">
    <%}else{%>
        <div style="width:551px; margin:auto;">
    <%} %>
        <table style="margin-top: 20px;">
            <tr>
                <th align="left" style="min-width:195px;">Resultado de validacion de CFDI</th>
                <% if(asc.getModuloActual() == MODULO.EVALUAR_PENDIENTE){ %>
                <td style="width:320px;"><select id="cfdiSelect">
                    <option value="V">Válido</option>
                    <option value="N" selected>No Válido</option>
                </select></td>
                <%}else { %>
                <td style="width:320px;"><%=solicitud.getEstatusCfdiDescripcion()==null?"":solicitud.getEstatusCfdiDescripcion() %></td>
                <%} %>
            </tr>
            <tr>
                <th align="left">Resultado de evaluación SOLICITUD</th>
                <% if(asc.getModuloActual() == MODULO.EVALUAR_PENDIENTE){ %>
                <td><select id="solicitudSelect">
                    <option value="A">Autorizada</option>
                    <option value="R" selected>Rechazada</option>
                </select></td>
                <%} else { %>
                <td ><%=solicitud.getEstatusCfdiDescripcion()==null?"":solicitud.getEstatusSolDescripcion() %></td>
                <%} %>
            </tr>
        </table>
    </div>
    <div>
        <ul style="display: flex; justify-content:space-around; padding:0 20% 0 20%; list-style:none">
            <% if(asc.getModuloActual() == MODULO.PENDIENTE_DISP_INSUFICIENTE){ %>
            <li><input type="submit" value="Validar disponibilidad y dar entrada" id="btnValidarDispEntrada" style="white-space: normal; max-width: 130px;"/></li>
            <%} else if (asc.getModuloActual() == MODULO.AUTORIZADA_ESPERA_DEP_IMPORTE){ %>
            <li><input type="submit" value="Dar validación de depósito del importe" id="btnValidarDepImporte" style="white-space: normal; max-width: 130px;"/></li>
            <li><input type="submit" value="Cancelar Solicitud" id="btnCancelarSolicitud" style="min-width:130px; min-height:30px;"/></li>
            <%} %>
            <li><input type="submit" value="Regresar" id="btnRegresar" style="min-width:130px; min-height:30px;"/></li>
        </ul>
    </div>
</form>
<script type="text/javascript">
    $(document).ready(function() {
        initValues();
        initMessageStatus();
        $("#btnRegresar").click(function (){
            <% if(asc.getModuloActual() == MODULO.EVALUAR_PENDIENTE){ %>
            $("#VALIDACION_CFDI").val($("#cfdiSelect").val());
            $("#EVALUACION_SOLICITUD").val($("#solicitudSelect").val());
            $('#alta').attr('action','<%=componentRequest.createComponentURL(componentRequest.getNode(), actualizarEvent)%>');
            $('#alta').submit();
            <%}else { %>
            $('#alta').attr('action','<%=componentRequest.createComponentURL(componentRequest.getNode(), fexitEvent)%>');
            $('#alta').submit();
            <%}%>
        });

        <% if(asc.getModuloActual() == MODULO.AUTORIZADA_ESPERA_DEP_IMPORTE){ %>
        $("#btnValidarDepImporte").click(function(){
            $('#alta').attr('action','<%=componentRequest.createComponentURL(componentRequest.getNode(), validarDepositoImporte)%>');
            $('#alta').submit();
        });
        $("#btnCancelarSolicitud").click(function(){
            $('#alta').attr('action','<%=componentRequest.createComponentURL(componentRequest.getNode(), cancelar)%>');
            $('#alta').submit();
        });
        <%}%>

        <% if(asc.getModuloActual() == MODULO.PENDIENTE_DISP_INSUFICIENTE){ %>
        $("#btnValidarDispEntrada").click(function (){
            $('#alta').attr('action','<%=componentRequest.createComponentURL(componentRequest.getNode(), actualizarEvent)%>');
            $('#alta').submit();
        });
        <%} %>

    });
    function initValues(){
        <% if(asc.getModuloActual() == MODULO.EVALUAR_PENDIENTE && null!=solicitud && null!=solicitud.getEstatusCfdiID()){%>
        $("#cfdiSelect").val("<%= solicitud.getEstatusCfdiID()%>");
        <%} if (asc.getModuloActual() == MODULO.EVALUAR_PENDIENTE && null!=solicitud && null!=solicitud.getEstatusSolID()){%>
        $("#solicitudSelect").val("<%= solicitud.getEstatusSolID()%>");
        <%}%>
    };

    function initMessageStatus(){
        var mensaje = "<%=asc.getMessage()%>";
        if(mensaje!=""){
            <%if(asc.getAccionActual()==ACCION.ACTUALIZAR){%>
            alert(mensaje);
            <% asc.setAccionActual(ACCION.INICIO);
            asc.setModuloActual(MODULO.ALL);
            asc.setEstatusActual(null);%>
            $('#alta').attr('action','<%=componentRequest.createComponentURL(componentRequest.getNode(), fexitEvent)%>');
            $('#alta').submit();
            <%}%>

            <%if(asc.getAccionActual()==ACCION.VALIDAR || asc.getAccionActual()== ACCION.CANCELAR){%>
            alert(mensaje);
            <% asc.setAccionActual(ACCION.INICIO);
            asc.setModuloActual(MODULO.ALL);
            asc.setEstatusActual(null);%>
            $('#alta').attr('action','<%=componentRequest.createComponentURL(componentRequest.getNode(), fexitEvent)%>');
            $('#alta').submit();
            <%}%>
        }
        var accionActual = <%=asc.getAccionActual()==ACCION.ACTUALIZAR%> ;
        if(accionActual && mensaje!=""){
            alert(mensaje);
            <% asc.setAccionActual(ACCION.INICIO);
            asc.setModuloActual(MODULO.ALL);%>
            $('#alta').attr('action','<%=componentRequest.createComponentURL(componentRequest.getNode(), fexitEvent)%>');
            $('#alta').submit();
        }
        accionActual = <%=asc.getAccionActual()==ACCION.VALIDAR%>;
        if(accionActual && mensaje!=""){
            console.log("Entra al metodo para mostrar el mensaje");
            alert(mensaje);
            //TODO Settear valores para regresar a la pantalla inicial
            $('#alta').attr('action','<%=componentRequest.createComponentURL(componentRequest.getNode(), fexitEvent)%>');
            $('#alta').submit();
        }
    }

</script>
</body>
</html>